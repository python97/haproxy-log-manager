#!/usr/bin/env python3.7
#
# File:   hlm.py
# Author: Michal Swiatkowski
# E-mail: michal@stopzone.pl
#
# Created on Jan 21, 2018, 12:20:33 PM
#
# HAPROXY LOG MANAGER
#
# v1.2


import sys
from sh import tail
import argparse
import re
import os
import time
import datetime
from random import choice


class Args(object):
    fname = None

    def __init__(self):
        self.aparse()

    def aparse(self):
        parser = argparse.ArgumentParser(prog='hlm', description='Haproxy Log Manager')
        parser.add_argument("-m", "--manage", action="store_true", default=False, help="manage haproxy log file")
        parser.add_argument("-t", "--tail", action="store_true", default=True, help="tail haproxy log file, display bad requests (default)")
        parser.add_argument("-a", "--all", action="store_true", default=False, help="tail haproxy log file, display all requests")
        parser.add_argument("-n", metavar='nr', type=int, nargs='?', default=0, help='number lines to tail, default 0')
        parser.add_argument("-s", metavar='search strings', type=str, nargs='?', help='searched strings in quotes, default empty')
        group = parser.add_argument_group('Subcommands for -s [search strings]')
        group.add_argument('-ua', '--ua', action='store_true', help="show User-Agent in standard output")
        parser.add_argument('filename', help='haproxy log file name. Allowed text formats')
        a = parser.parse_args()

        Args.fname = a.filename
        if (a.tail or a.all) and not a.manage:
            new_tail = Manager(Args.fname)
            for l in new_tail.tail_file(a.n):
                line = l.split()
                if len(line) > 8:

                    if a.s is None:
                        if "failure" in line:
                            data = line[-5]
                            ip = line[-6]
                            backend = line[-4]
                            failure = line[-3] + " " + line[-2] + " " + line[-1]
                            new_tail.printColored("{} {} {} {}".format(data, ip, backend, failure))
                        else:
                            if len(line) > 12:
                                if len(line[12]) == 3 and re.match('\\d{3}', line[12]):
                                    scode = line[12]
                                    data = line[8]
                                    ip = line[7]
                                    backend2 = line[10]
                                    url = ' '.join(line[-3:])
                                elif len(line[11]) == 3 and re.match('\\d{3}', line[11]):
                                    scode = line[11]
                                    data = line[7]
                                    ip = line[6]
                                    backend2 = line[9]
                                    url = ' '.join(line[-3:])
                                else:
                                    scode = False
                            else:
                                scode = False

                            if scode:
                                if a.all:
                                    new_tail.printColored("{} {} {}".format(data, ip, backend2), endl=" ")

                                    if int(scode) in range(200, 400):
                                        new_tail.printColored("{}".format(scode), "green", endl=" ")
                                    elif int(scode) in range(400, 500):
                                        new_tail.printColored("{}".format(scode), "magenta", endl=" ")
                                    else:
                                        new_tail.printColored("{}".format(scode), "red", endl=" ")

                                    new_tail.printColored("{}".format(url))
                                else:
                                    if int(scode) not in range(200, 400):
                                        new_tail.printColored("{} {} {}".format(data, ip, backend2), endl=" ")
                                        if int(scode) in range(404, 500):
                                            new_tail.printColored("{}".format(scode), "magenta", endl=" ")
                                        else:
                                            new_tail.printColored("{}".format(scode), "red", endl=" ")
                                        new_tail.printColored("{}".format(url))
                            else:
                                new_tail.printColored("{} {} {}".format(ip.split(":")[0], backend2, url), "magenta", endl="\n")
                                # new_tail.printColored("{}".format(l[:-1]), "red", endl="\n")

                    else:
                        if len(a.s) < 3:
                            new_tail.printColored("Type min. 3 chars is search string.", "red")
                            sys.exit()
                        else:
                            test = new_tail.search_in_line(a.s, l)
                            if len(test) > 0:
                                if len(line) <= 20:
                                    if "failure" not in line:
                                        new_tail.printColored("{}".format(' '.join(line)), "red")
                                else:
                                    if a.ua is False:
                                        data = ' '.join(line[-3:])
                                    else:
                                        data = ' '.join(line[19:])

                                    if len(line[12]) == 3 and re.match('\\d{3}', line[12]):
                                        scode = line[12]
                                        date = line[8]
                                        ip = line[7]
                                        backend = line[10]
                                    elif len(line[11]) == 3 and re.match('\\d{3}', line[11]):
                                        scode = line[11]
                                        date = line[7]
                                        ip = line[6]
                                        backend = line[9]
                                    else:
                                        scode = None

                                    if scode is not None:
                                        new_tail.printColored("{} {} {}".format(date, ip, backend), endl=" ")
                                        if int(scode) in range(200, 400):
                                            new_tail.printColored("{}".format(scode), "green", endl=" ")
                                        else:
                                            new_tail.printColored("{}".format(scode), "red", endl=" ")
                                        print("{}".format(data))
                                    else:
                                        new_tail.printColored("{}".format(' '.join(line)), "red")

        elif a.manage:
            new_manage = Manager(Args.fname)
            new_manage.show_menu()

        else:
            parser.print_help()


class Manager:
    exit_code = 0
    start_date = 0
    end_date = 0
    time_now = time.time()
    position = 0

    def __init__(self, file_name):
        self.file_name = file_name

    def tail_file(self, lines):
        if os.path.isfile(self.file_name):
            data = os.path.splitext(self.file_name)
            if data[1] not in [".tgz", ".tar", ".bz2", ".gz"]:
                while True:
                    try:
                        for line in tail("-f", "-{}".format(lines), self.file_name, _iter=True):
                            yield line
                    except KeyboardInterrupt:
                        sys.exit()
                    except BrokenPipeError:
                        sys.exit()
                    except Exception as e:
                        print("Exception 1")
                        print("{}: {}".format(type(e).__name__, e))
                        sys.exit(1)
            else:
                self.printColored("Forbidden file format", "red")
        else:
            self.printColored("File not found", "red")

    def get_lines(self):
        with open(self.file_name) as f:
            tdate = datetime.date.today()
            for l in f:
                line = l.split()
                cdate = time.mktime(datetime.datetime.strptime(str(tdate) + ' ' + line[2], "%Y-%m-%d %H:%M:%S").timetuple())
                if Manager.start_date <= cdate <= Manager.end_date:
                    yield l

                if cdate > Manager.end_date + 10:
                    break

    def get_ips_count(self):
        file_date = ""
        more_data = {}
        hours = []
        deleted_lines = []
        for l in self.get_lines():
            line = l.split()
            file_date = line[0] + "_" + line[1]
            if len(line) >= 12:
                if "from" in line:
                    ip = line[7].split(":")
                    scode = line[12]
                else:
                    ip = line[6].split(":")
                    scode = line[11]

                if ip[0] in more_data:
                    more_data[ip[0]]["all_requests"] += 1
                else:
                    more_data[ip[0]] = {}
                    more_data[ip[0]]["all_requests"] = 1

                if (len(scode) == 3) and re.match('\\d{3}', scode) and (len(line) >= 19):
                    if scode in more_data[ip[0]]:
                        more_data[ip[0]][scode] += 1
                    else:
                        more_data[ip[0]][scode] = 1
                else:
                    if "wrong" in more_data[ip[0]]:
                        more_data[ip[0]]["wrong"] += 1
                    else:
                        more_data[ip[0]]["wrong"] = 1

                hours.append(line[2])
            else:
                deleted_lines.append(l)

        if len(more_data) == 0:
            self.printColored("No data for selected time period.", "red")
            return 0

        sorted_d = sorted(more_data, key=lambda x: (more_data[x]["all_requests"]), reverse=True)

        if len(sorted_d) < 50:
            for v in sorted_d:
                print("{}\t".format(v), end="")
                print("[", end=" ")
                for s in more_data[v]:
                    self.printColored("{}".format(s), "red", endl="")
                    print(":{}".format(more_data[v][s]), end=" ")
                print("]")
        else:
            out_filename = "lista_adresow_ip_{}_{}_{}.txt".format(file_date, datetime.date.today(), choice(range(200)))
            if os.path.isfile(out_filename):
                self.printColored("File {} exists. Aborting ...".format(out_filename), "red")
            else:
                myfile = open(out_filename, 'w')
                myfile.write("##### Requests per IP from {} to {} | {}\n".format(min(hours), max(hours), file_date))
                for i in sorted_d:
                    data_file = "{}\t[ ".format(i)
                    for s in more_data[i]:
                        data_file += "{}:{} ".format(s, more_data[i][s])
                    myfile.write("{}]\n".format(data_file))

                if len(deleted_lines) != 0:
                    myfile.write("\n\n################ SKIPPED LINES ################\n\n")
                    for d in deleted_lines:
                        myfile.write("{}\n".format(d))

                if os.path.isfile(out_filename):
                    self.printColored("File", "green", endl=" ")
                    self.printColored("{}".format(out_filename), "yellow", endl=" ")
                    self.printColored("was successfully created!", "green")
                else:
                    self.printColored("Can't create file.", "red")

    def get_req_count(self):
        hreq = {}
        file_date = ""
        deleted_lines = []
        for l in self.get_lines():
            line = l.split()
            file_date = line[0] + "_" + line[1]
            t = line[2].split(":")
            lpattern = "{}:{}".format(t[0], t[1])
            if len(line) >= 12:
                if "from" in line:
                    scode = line[12]
                    bytes_read = 0
                else:
                    scode = line[11]
                    bytes_read = 0

                if (len(scode) == 3) and re.match('\\d{3}', scode) and (len(line) >= 10):

                    if "from" in line:
                        bytes_read = int(line[13])
                    else:
                        bytes_read = int(line[12])

                    if lpattern in hreq:
                        if "requests" in hreq[lpattern]:
                            hreq[lpattern]["requests"] += 1
                        else:
                            hreq[lpattern]["requests"] = 1

                        if scode in hreq[lpattern]:
                            hreq[lpattern][scode] += 1
                        else:
                            hreq[lpattern][scode] = 1

                        if "bytes_read" in hreq[lpattern]:
                            hreq[lpattern]["bytes_read"] += bytes_read
                        else:
                            hreq[lpattern]["bytes_read"] = bytes_read
                    else:
                        hreq[lpattern] = {}
                        if "requests" in hreq[lpattern]:
                            hreq[lpattern]["requests"] += 1
                        else:
                            hreq[lpattern]["requests"] = 1

                        if scode in hreq[lpattern]:
                            hreq[lpattern][scode] += 1
                        else:
                            hreq[lpattern][scode] = 1

                        if "bytes_read" in hreq[lpattern]:
                            hreq[lpattern]["bytes_read"] += bytes_read
                        else:
                            hreq[lpattern]["bytes_read"] = bytes_read
                else:
                    if lpattern in hreq:
                        if "requests" in hreq[lpattern]:
                            hreq[lpattern]["requests"] += 1
                        else:
                            hreq[lpattern]["requests"] = 1

                        if "wrong" in hreq[lpattern]:
                            hreq[lpattern]["wrong"] += 1
                        else:
                            hreq[lpattern]["wrong"] = 1
                    else:
                        hreq[lpattern] = {}
                        if "requests" in hreq[lpattern]:
                            hreq[lpattern]["requests"] += 1
                        else:
                            hreq[lpattern]["requests"] = 1

                        if "wrong" in hreq[lpattern]:
                            hreq[lpattern]["wrong"] += 1
                        else:
                            hreq[lpattern]["wrong"] = 1
            else:
                deleted_lines.append(l)

        if len(hreq) == 0:
            self.printColored("No data for selected time period.", "red")
            return 0

        """
            Ilosc requestow / czas [min/s]
            Ilosc przeslanych danych B/req
        """
        all_req = len(hreq)
        acreq = 0
        bread = 0
        wrong_req = 0
        for i in hreq:
            acreq += hreq[i]["requests"]
            if "bytes_read" in hreq[i]:
                bread += hreq[i]["bytes_read"]
            if "wrong" in hreq[i]:
                wrong_req += hreq[i]["wrong"]

        avg_req_min = round((int(acreq) / all_req), 1)
        avg_req_sek = round((int(acreq) / all_req / 60), 1)
        avg_bytes_req = round((bread / (acreq - wrong_req)), 2)
        """
            Koniec
        """

        if all_req < 50:
            for i in hreq:
                print("{}\t {} req \t {} bytes read".format(i, hreq[i]["requests"], hreq[i]["bytes_read"]), end="\t")
                print("[", end=" ")
                for s in hreq[i]:
                    if s != "requests" and s != "bytes_read":
                        self.printColored("{}".format(s), "red", endl="")
                        print(":{}".format(hreq[i][s]), end=" ")
                print("]")
            print()
            self.printColored("\t#### PODSUMOWANIE ####", "grey")
            self.printColored("\t # Ilosc requestow : {}".format(acreq), "magenta")
            self.printColored("\t # Ilosc pozycji : {}".format(all_req), "magenta")
            self.printColored("\t # Srednia ilosc requestow na minute : {}".format(avg_req_min), "magenta")
            self.printColored("\t # Srednia ilosc requestow na sekunde : {}".format(avg_req_sek), "magenta")
            self.printColored("\t # Srednia ilosc bytow na request : {} bytes".format(avg_bytes_req), "magenta")
            self.printColored("\t#### KONIEC ####", "grey")
        else:
            out_filename = "requesty_{}_{}_{}.txt".format(file_date, datetime.date.today(), choice(range(200)))
            if os.path.isfile(out_filename):
                self.printColored("File {} exists. Aborting ...".format(out_filename), "red")
            else:
                myfile = open(out_filename, 'w')
                myfile.write("##### Requests from {} to {} | {}\n".format(list(hreq.keys())[0], list(hreq.keys())[-1], file_date))
                for i in hreq:
                    data_file = "{}\t {} req \t {} bytes read\t[ ".format(i, hreq[i]["requests"], hreq[i]["bytes_read"])
                    for s in hreq[i]:
                        if s != "requests" and s != "bytes_read":
                            data_file += "{}:{} ".format(s, hreq[i][s])
                    myfile.write("{}]\n".format(data_file))

                myfile.write("\n")
                myfile.write("\t#### PODSUMOWANIE ####\n")
                myfile.write("\t # Ilosc requestow : {}\n".format(acreq))
                myfile.write("\t # Ilosc pozycji : {}\n".format(all_req))
                myfile.write("\t # Srednia ilosc requestow na minute : {}\n".format(avg_req_min))
                myfile.write("\t # Srednia ilosc requestow na sekunde : {}\n".format(avg_req_sek))
                myfile.write("\t # Srednia ilosc bytow na request : {} bytes\n".format(avg_bytes_req))
                myfile.write("\t#### KONIEC ####\n")
                myfile.write("\n")

                if len(deleted_lines) != 0:
                    myfile.write("\n\n################ SKIPPED LINES ################\n\n")
                    for d in deleted_lines:
                        myfile.write("{}\n".format(d))

                if os.path.isfile(out_filename):
                    self.printColored("File", "green", endl=" ")
                    self.printColored("{}".format(out_filename), "yellow", endl=" ")
                    self.printColored("was successfully created!", "green")
                else:
                    self.printColored("Can't create file.", "red")

    def find_user_agent(self, line, words):
        wlist = words.split(" ")
        for word in wlist:
            if line.find(word) != -1:
                return True
        return False

    def search_ua(self, ipaddress=False, ua=False):
        out_list = []
        """
            Format logow:
                1 : timestamp IP backend/frontend status_code {User-Agent} "method url http/1.1"
                2 : timestamp {User-Agent} url
        """
        out_form = 1
        while True:
            self.printColored(" 1 : timestamp IP backend/frontend status_code {User-Agent} \"method url http/1.1\"", "red")
            self.printColored(" 2 : timestamp {User-Agent} url", "red")
            print()
            tmp = input("Select output format [1/2] > ")
            print()
            if tmp == "1":
                out_form = 1
                break
            elif tmp == "2":
                out_form = 2
                break
            else:
                continue

        for l in self.get_lines():
            line = l.split()
            file_date = line[0] + "_" + line[1]
            if len(line) > 15:
                if "from" in line:
                    ipa = line[7].split(":")
                    ip = ipa[0]
                    scode = line[12]
                    if out_form == 1:
                        form = "{} {} {} {} {}".format(line[8], ip, line[10], line[12], " ".join(line[19:]))
                    else:
                        timest = int(time.mktime(datetime.datetime.strptime(line[8], "[%d/%b/%Y:%H:%M:%S.%f]").timetuple()))
                        form = "{} {} {}".format(timest, " ".join(line[19:-3]), line[-2])
                    ua_start = line[19]
                    lenline = 23
                else:
                    ipa = line[6].split(":")
                    ip = ipa[0]
                    scode = line[11]
                    if out_form == 1:
                        form = "{} {} {} {} {}".format(line[7], ip, line[9], line[11], " ".join(line[18:]))
                    else:
                        timest = int(time.mktime(datetime.datetime.strptime(line[7], "[%d/%b/%Y:%H:%M:%S.%f]").timetuple()))
                        form = "{} {} {}".format(timest, " ".join(line[18:-3]), line[-2])
                    ua_start = line[18]
                    lenline = 22

                if not ua:
                    if len(line) > lenline:
                        if ipaddress is False:
                            out_list.append(form)
                        else:
                            if ipaddress == ip and len(ua_start) != 0:
                                out_list.append(form)
                else:
                    if len(line) > lenline:
                        if ipaddress is False and self.find_user_agent(form, ua):
                            out_list.append(form)
                        else:
                            if ipaddress == ip and len(ua_start) != 0 and self.find_user_agent(form, ua):
                                out_list.append(form)

        array_size = len(out_list)

        if array_size == 0:
            self.printColored("The result is empty :(", "red")
            return 0

        if array_size < 100:
            for i in out_list:
                print(i)
        else:
            out_filename = "user_agents_{}_{}.txt".format(datetime.date.today(), choice(range(200)))
            if os.path.isfile(out_filename):
                self.printColored("File {} exists. Aborting ...".format(out_filename), "red")
            else:
                myfile = open(out_filename, 'w')
                myfile.write("")
                myfile.write("##### User-Agents list\n")
                myfile.write("##### IP : {}\n".format(ipaddress))
                myfile.write("##### Searched words : {}\n".format(ua))
                myfile.write("##### Searched file : {}\n".format(Args.fname))
                start, end = self.human_date()
                myfile.write("##### Time : {} - {}\n".format(start, end))
                for c, i in enumerate(out_list, 1):
                    # myfile.write("{}. {}\n".format(c, i))
                    myfile.write("{}\n".format(i))

                if os.path.isfile(out_filename):
                    self.printColored("File", "green", endl=" ")
                    self.printColored("{}".format(out_filename), "yellow", endl=" ")
                    self.printColored("was successfully created!", "green")

    def set_range(self):
        Manager.position = 1
        self.clear_range()
        hstart = input("Enter start time [00:00:00]: > ")
        hend = input("Enter end time [23:59:59]: > ")
        print()
        try:
            if len(hstart) == 0:
                hstart = "00:00:00"
            if len(hend) == 0:
                hend = "23:59:59"

            date = datetime.date.today()
            Manager.start_date = int(time.mktime(datetime.datetime.strptime(str(date) + ' ' + hstart, "%Y-%m-%d %H:%M:%S").timetuple()))
            Manager.end_date = int(time.mktime(datetime.datetime.strptime(str(date) + ' ' + hend, "%Y-%m-%d %H:%M:%S").timetuple()))
        except ValueError:
            self.set_range()
        except KeyboardInterrupt:
            sys.exit()

    def human_date(self):
        start = datetime.datetime.fromtimestamp(Manager.start_date).strftime('%Y-%m-%d %H:%M:%S')
        end = datetime.datetime.fromtimestamp(Manager.end_date).strftime('%Y-%m-%d %H:%M:%S')
        s = start.split(" ")
        e = end.split(" ")
        return (s[1], e[1])

    def clear_range(self):
        Manager.start_date = ""
        Manager.end_date = ""

    def ipc(self):
        self.set_range()
        Manager.time_now = time.time()
        self.get_ips_count()

    def req(self):
        self.set_range()
        Manager.time_now = time.time()
        self.get_req_count()

    def ipv4_validator(self, ipa):
        ip = ipa.split(".")
        if len(ip) == 4:
            for i in ip:
                if int(i) not in range(256):
                    return False
        else:
            return False
        return True

    def uag(self):
        self.set_range()
        ip = input("IP [0.0.0.0] > ")
        print()
        ua = input("User-Agent - list of words [] > ")
        print()
        try:
            if len(ip) == 0 or ip == "0.0.0.0":
                ipaddr = False
            else:
                if self.ipv4_validator(ip):
                    ipaddr = ip
                else:
                    self.printColored("Wrong IP address format", "red")
                    self.uag()

            if len(ua) == 0:
                ua = False

            Manager.time_now = time.time()
            self.search_ua(ipaddr, ua)

        except KeyboardInterrupt:
            sys.exit()
        except Exception as e:
            self.printColored("{}".format(e), "red")
            self.uag()

    def search_in_line(self, search_words, line):
        lines = []
        out = []
        search = search_words.split()
        search_len = len(search)
        count = 0
        while count < search_len:
            if count == 0:
                if search[count].lower() in line.lower():
                    lines.append(line)
            else:
                for val in lines:
                    if search[count].lower() in val.lower():
                        out.append(val)

            count += 1

        if search_len == 1:
            return lines
        else:
            return out

    def search_phrase(self):
        self.set_range()
        print()
        search_tmp = ""
        while True:
            search_tmp = input("Searched words [] > ")
            if len(' '.join(search_tmp.split())) < 3:
                print("Type min. 4 chars")
                print()
                continue
            else:
                break

        Manager.time_now = time.time()

        out = []
        for l in self.get_lines():
            tmp = self.search_in_line(search_tmp, l)
            if len(tmp) > 0:
                out.append(tmp)

        if len(out) > 0:
            out_filename = "searched_words_{}_{}.txt".format(datetime.date.today(), choice(range(200)))
            if os.path.isfile(out_filename):
                self.printColored("File {} exists. Aborting ...".format(out_filename), "red")
            else:
                start, end = self.human_date()
                myfile = open(out_filename, 'w')
                myfile.write("\n")
                myfile.write("##### Searched words: {}\n".format(search_tmp))
                myfile.write("##### Hours range: {} - {}\n".format(start, end))
                myfile.write("##### Num of lines : {}\n".format(len(out)))
                myfile.write("\n")
                for c in out:
                    myfile.write(' '.join(c))

                if os.path.isfile(out_filename):
                    print()
                    self.printColored("File", "green", endl=" ")
                    self.printColored("{}".format(out_filename), "yellow", endl=" ")
                    self.printColored("was successfully created!", "green")

        else:
            print()
            self.printColored("The result is empty :(", "red")
            return 0

    def show_menu(self):
        while True:
            try:
                print()
                print("### Available options ###")
                print("\t ipc \t\t count IP in time range")
                print("\t req \t\t count requests in time range")
                print("\t search \t search phrase in log")
                print("\t uag \t\t list of User-Agents per IP/User-Agent")
                print("\t quit \t\t stop the program")
                print()
                if Manager.position != 0:
                    self.printColored("\t Wykonanao w {}s".format(round((time.time() - Manager.time_now), 3)), "yellow")
                    Manager.position = 0
                    print()
                option = input("HaproxyLogManager$ ")

                options = {
                    "quit": self.quit,
                    "ipc": self.ipc,
                    "req": self.req,
                    "uag": self.uag,
                    "search": self.search_phrase
                }

                options[option]()

                if Manager.exit_code == 666:
                    break

            except KeyError:
                continue
            except KeyboardInterrupt:
                print()
                self.quit()
                sys.exit()
            except EOFError:
                sys.exit()
            except Exception as e:
                print("Exception 2")
                print("{}: {}".format(type(e).__name__, e))
                sys.exit(1)

    def printColored(self, text, color="default", endl="\n"):
        COLOR = {
            'blue': '\033[94m',
            'default': '\033[99m',
            'grey': '\033[90m',
            'yellow': '\033[93m',
            'black': '\033[90m',
            'cyan': '\033[96m',
            'green': '\033[92m',
            'magenta': '\033[36m',
            'white': '\033[97m',
            'red': '\033[91m'
        }
        try:
            if color in COLOR:
                color = COLOR[color]
            print(color + "{}".format(text) + "\033[0m", end=endl)
        except KeyboardInterrupt:
            sys.exit()

    def quit(self):
        print("c u later, alligator.")
        Manager.exit_code = 666


def main():
    Args()


if __name__ == "__main__":
    main()
